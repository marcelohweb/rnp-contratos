pragma solidity ^0.5.0;

contract CaixaDeMensagem{

    string public mensagem;

    constructor(string memory mensagemInicial) public{
        mensagem = mensagemInicial;
    }

    function setMensagem(string memory novaMensagem) public {
        mensagem = novaMensagem;
    }

    function getMensagem() public view returns(string memory){
        return mensagem;
    }

}
