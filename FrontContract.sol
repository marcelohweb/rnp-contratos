pragma solidity ^0.5.0;

contract FrontContract{

    address owner;

    address public activeContractAddress;

    constructor(address active) public{
        owner = msg.sender;
        activeContractAddress = active;
    }

    function setActiveContract(address active) public{
        require(owner == msg.sender, "No privileges");

        activeContractAddress = active;
    }

}
