pragma solidity ^0.5.0;

contract RNPToken{

    address public minerador;
    mapping(address => uint) public saldo;

    constructor() public{
        minerador = msg.sender;
    }

    function minerar(address favorecido, uint valor) public {

        require(msg.sender == minerador, "No privileges");
        saldo[favorecido] += valor;

    }

    function enviar(address favorecido, uint valor) public{

        require(saldo[msg.sender] >= valor, "Sem saldo");
        saldo[msg.sender] -= valor;
        saldo[favorecido] += valor;

    }

    function getSaldo(address account) public view returns (uint){

        require(msg.sender == minerador || msg.sender == account, "No privileges");
        return saldo[account];

    }

    function getSaldo() public view returns (uint){

        return saldo[msg.sender];

    }

}
