pragma solidity >=0.5.0 <0.7.0;

contract Owned{

    uint public value;

    address public owner;

    constructor(uint _value) internal{
        owner = msg.sender;
        value = _value;
    }

}

contract SoliditySintax is Owned{

    uint public uintVar = 10;

    int public intVar;

    bool public booleanVar;

    address public addressVar;

    address payable public addressPayableVar;

    string public stringVar;

    address[] public uintVectorVar;

    struct Subject{
        string cpf;
        string nome;
    }

    mapping(address => string) public addresEntities;

    //Opcional
    constructor() public Owned(10){}

    function setUintVar(uint valor) public {
        uintVar = valor;
    }

    function getUintVar() public view returns (uint) {
        return uintVar;
    }

    function setBooleanVar(bool valor) public {
        booleanVar = valor;
    }

    function isBooleanVar() public view returns (bool) {
        return booleanVar;
    }

    function setAddressVar(address valor) public {
        addressVar = valor;
    }

    function getAddressVar() public view returns (address) {
        return addressVar;
    }

    function getAddressBalance() public view returns(uint){
        return addressVar.balance;
    }

    function setAaddressPayableVar(address payable valor) public {
        addressPayableVar = valor;
    }

    function getAddressPayableVar() public view returns (address payable) {
        return addressPayableVar;
    }

    function getPaybleAddressBalance() public view returns(uint){
        return addressPayableVar.balance;
    }

    function setStringVar(string memory valor) public {
        stringVar = valor;
    }

    function getStringValue() public view returns(string memory){
        return stringVar;
    }

    //Indica que uma quantidade de ether deve ser passada
    function payToAccount() public payable{
        require(owner == msg.sender, "Sem permissão");
        addressPayableVar.transfer(msg.value);
    }

    //pure indica que não haverá leitura de estado armazenado
    function getMultipleTypes() public pure returns (uint, bool){
        return (7, true);
    }

    function addValueToVector(address valor) public {
        uintVectorVar.push(valor);
    }

    function getValueFromVectorVar(uint index) public view returns(address){
        return uintVectorVar[index];
    }

    function addAddressEntity(address endereco, string memory nome) public{
        addresEntities[endereco] = nome;
    }

    function getAddressEntityByAddress(address _address) view public returns (address, string memory){
        return (_address, addresEntities[_address]);
    }

    function myPrivateFunction() private pure{
        Subject memory myStruct = Subject("00054877745","Marcelo Soares");

        //ou

        Subject memory myStruct2;
        myStruct2.cpf = "21314215456";
        myStruct2.nome = "Marcelo Soares";
    }

    /*
        Variáveis e funções especiais

        blockhash(uint blockNumber) returns (bytes32): hash of the given block - only works for 256 most recent, excluding current, blocks
        block.coinbase (address payable): current block miner’s address
        block.difficulty (uint): current block difficulty
        block.gaslimit (uint): current block gaslimit
        block.number (uint): current block number
        block.timestamp (uint): current block timestamp as seconds since unix epoch
        gasleft() returns (uint256): remaining gas
        msg.data (bytes calldata): complete calldata
        msg.sender (address payable): sender of the message (current call)
        msg.sig (bytes4): first four bytes of the calldata (i.e. function identifier)
        msg.value (uint): number of wei sent with the message
        now (uint): current block timestamp (alias for block.timestamp)
        tx.gasprice (uint): gas price of the transaction
        tx.origin (address payable): sender of the transaction (full call chain)
    */

}
