pragma solidity ^0.5.0;

contract CaixaDeMensagemV2{

    address owner;

    bool isActive;

    string public mensagem;

    constructor(string memory mensagemInicial) public{
        owner = msg.sender;
        mensagem = mensagemInicial;
        isActive = true;
    }

    function setActive(bool ativo) public {
        require(msg.sender == owner, "No privileges");
        isActive = ativo;
    }

    function setMensagem(string memory novaMensagem) public {
        require(isActive == true, "This contract is disabled. Call 0xXYZ.activeContractAddress to get the active contract");
        mensagem = novaMensagem;
    }

    function getMensagem() public view returns(string memory){
        require(isActive == true, "This contract is disabled. Call 0xXYZ.activeContractAddress to get the active contract");
        return mensagem;
    }

}
