pragma solidity ^0.5.1;

contract Loteria{

    address public gerente;

    address payable[] public jogadores;

    constructor() public{
        gerente = msg.sender;
    }

    function participar() public payable{
        require(msg.value == 0.1 ether, "Para participar envie 0.1 ether");
        jogadores.push(msg.sender);
    }

    function random() public view returns (uint){
        return uint(keccak256(abi.encodePacked(block.difficulty, now, jogadores)));
    }

    function escolherVencedor() public returns(address vencedor){
        require (msg.sender == gerente, "Sem privilégio de gerente");

        uint index = random() % jogadores.length;
        address payable addressVencedor = jogadores[index];
        addressVencedor.transfer(address(this).balance);
        jogadores = new address payable[](0);

        return addressVencedor;
    }

    function totalJogadores() public view returns (uint){
        return jogadores.length;
    }

    function totalApostas() public view returns(uint){
        return address(this).balance;
    }

    function testRandom() public view returns (uint){

        uint index = random() % jogadores.length;
        return index;

    }

}
